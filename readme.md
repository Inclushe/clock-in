# Clock In

```bash
pnpm i
npm run watch
```

## Issues with Other Apps

- The Windows 10 Clock app is garbage
- Online clocks are ugly and/or too small

## Philosophies

- The app should be bold and playful.
- The app should have a defining symbol to differentiate from other clock apps.
- The clock itself should be as large as aesthetically possible.

## Todo

- [ ] Clean up code
  - [ ] State should not be mutated outside mutation handlers (strict: true)
    - Potential drawback is that components are destroyed
  - [ ] Fix autosave feature that was put in last-minute
- [ ] Re-enable alarm
- [ ] Why is timer initially slow?
